import express, { json } from "express";
import cors from "cors";
import { usuarios } from "./routes";

const app = express();

app.use(cors());
app.use(json());

app.use("/usuarios", usuarios);
