import { Router } from "express";
import {
  addUsuarios,
  deleteUsuarios,
  getUsuarios,
  updateUsuarios,
} from "../controllers/usuarios";

const router = Router();

router.get("/", getUsuarios);

router.post("/", addUsuarios);

router.put("/", updateUsuarios);

router.delete("/", deleteUsuarios);

export default router;
